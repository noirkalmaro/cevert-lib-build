
'use strict'

if (process.env.NODE_ENV === 'production') {
  module.exports = require('./cevert-lib.cjs.production.min.js')
} else {
  module.exports = require('./cevert-lib.cjs.development.js')
}
