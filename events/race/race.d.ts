import { Session } from "../../entities/session/session";
import { SessionParticipant } from "../../entities/sessionParticipant/sessionParticipant";
import { Track } from "../../entities/track/track";
export declare class Race extends Session {
    readonly sessionParticipants: SessionParticipant[];
    readonly track: Track;
    private valuesProvider;
    constructor(sessionParticipants: SessionParticipant[], track: Track);
    prepareGrid(): void;
    doRace(): void;
    protected printLap(num: number): void;
    protected collisionChecker(participant1: SessionParticipant, participant2: SessionParticipant): boolean;
}
