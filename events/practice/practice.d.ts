import { Session } from "../../entities/session/session";
import { Team } from "../../entities/team/team";
import { Track } from "../../entities/track/track";
export declare class Practice extends Session {
    readonly teams: Team[];
    readonly track: Track;
    constructor(teams: Team[], track: Track);
    doPractice(): void;
}
