export declare enum SessionTypes {
    Practice = "practice",
    Qualification = "qualification",
    Race = "race"
}
