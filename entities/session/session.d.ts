import { SessionParticipant } from "../sessionParticipant/sessionParticipant";
import { Track } from "../track/track";
export declare class Session {
    readonly name: string;
    readonly track: Track;
    readonly randomRange: [number, number];
    sessionParticipants: SessionParticipant[];
    constructor(name: string, track: Track, randomRange: [number, number], sessionParticipants: SessionParticipant[]);
    sortByBest(): void;
    sortByTotal(): void;
    traceResults(): string;
}
