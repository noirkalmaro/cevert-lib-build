export declare class Driver {
    readonly id: string;
    readonly name: string;
    readonly number: number;
    readonly power: number;
    constructor(id: string, name: string, number: number, power: number);
    traceDriverInfo(): string;
}
