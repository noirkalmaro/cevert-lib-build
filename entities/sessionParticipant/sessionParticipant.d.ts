import { SessionTypes } from "../../events/events.enum";
import { Car } from "../car/car";
import { Driver } from "../driver/driver";
import { Lap } from "../lap/lap";
import { Team } from "../team/team";
export declare class SessionParticipant {
    readonly car: Car;
    readonly driver: Driver;
    readonly team: Team;
    status: boolean;
    currentResult: number;
    results: number[];
    retirementLap: number;
    private total;
    private best;
    private laps;
    private valuesProvider;
    private strategy;
    constructor(car: Car, driver: Driver, team: Team);
    addLap(lap: Lap): void;
    overtakeSuccess(): void;
    wasOvertaken(): void;
    overtakeFailed(): void;
    verifyRetirement(sessionType: SessionTypes): void;
    getTotal: () => number;
    getBestValue: () => number;
    getBest: () => {};
    getLaps: () => Lap[];
}
