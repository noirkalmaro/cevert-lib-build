import { SessionParticipant } from "../sessionParticipant/sessionParticipant";
import { Track } from "../track/track";
export declare class Lap {
    readonly num: number;
    readonly sessionParticipant: SessionParticipant;
    readonly track: Track;
    private lapResult;
    private valuesProvider;
    constructor(num: number, sessionParticipant: SessionParticipant, track: Track);
    doPracticeLap(): void;
    doQualificationLap(): void;
    doGridLap(index: number): void;
    doRaceLap(): void;
    overtakeFailed(): void;
    overtakeSuccessPenalty(): void;
    overtakeTargetPenalty(): void;
    doPitStop(): void;
    result(): number;
}
