import { Car } from "../car/car";
import { Driver } from "../driver/driver";
export declare class Team {
    readonly id: string;
    readonly name: string;
    readonly car: Car;
    readonly driver1: Driver;
    readonly driver2: Driver;
    constructor(id: string, name: string, car: Car, driver1: Driver, driver2: Driver);
    traceTeamInfo(): string;
}
