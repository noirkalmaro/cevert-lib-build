export declare class Car {
    readonly id: string;
    readonly name: string;
    readonly aero: number;
    readonly chassis: number;
    readonly engine: number;
    readonly reliability: number;
    constructor(id: string, name: string, aero: number, chassis: number, engine: number, reliability: number);
    traceCarInfo(): string;
}
