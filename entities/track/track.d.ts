import { TechConstants } from "../../types/techMultConstants/techMultConstants";
export declare class Track {
    readonly id: string;
    readonly name: string;
    readonly aero: TechConstants;
    readonly chassis: TechConstants;
    readonly engine: TechConstants;
    constructor(id: string, name: string, aero: TechConstants, chassis: TechConstants, engine: TechConstants);
    traceTrackInfo(): string;
}
