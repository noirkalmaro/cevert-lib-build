'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

var Car = /*#__PURE__*/function () {
  function Car(id, name, aero, chassis, engine, reliability) {
    this.id = id;
    this.name = name;
    this.aero = aero;
    this.chassis = chassis;
    this.engine = engine;
    this.reliability = reliability;
  }

  var _proto = Car.prototype;

  _proto.traceCarInfo = function traceCarInfo() {
    return this.name + " id: " + this.id + " aero: " + this.aero + " chassis: " + this.chassis + " engine: " + this.engine;
  };

  return Car;
}();

var Driver = /*#__PURE__*/function () {
  function Driver(id, name, number, power) {
    this.id = id;
    this.name = name;
    this.number = number;
    this.power = power;
  }

  var _proto = Driver.prototype;

  _proto.traceDriverInfo = function traceDriverInfo() {
    return this.name + " #" + this.number + " id: " + this.id + " power: " + this.power;
  };

  return Driver;
}();

function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

function _inheritsLoose(subClass, superClass) {
  subClass.prototype = Object.create(superClass.prototype);
  subClass.prototype.constructor = subClass;

  _setPrototypeOf(subClass, superClass);
}

function _setPrototypeOf(o, p) {
  _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
    o.__proto__ = p;
    return o;
  };

  return _setPrototypeOf(o, p);
}

var SessionTypes;

(function (SessionTypes) {
  SessionTypes["Practice"] = "practice";
  SessionTypes["Qualification"] = "qualification";
  SessionTypes["Race"] = "race";
})(SessionTypes || (SessionTypes = {}));

var Injector = /*#__PURE__*/function () {
  function Injector() {
    this.serviceInstanceMap = {};
  }

  var _proto = Injector.prototype;

  _proto.resolve = function resolve(name) {
    var res = this.serviceInstanceMap[name];

    if (!res) {
      throw new Error(name + " service wasn't found");
    }

    return res;
  };

  _proto.register = function register(name, entity) {
    this.serviceInstanceMap[name] = entity;
  };

  return Injector;
}();

var injector = /*#__PURE__*/new Injector();

var Strategy = function Strategy(pitLap) {
  this.pitLap = pitLap;
};

var SessionParticipant = /*#__PURE__*/function () {
  function SessionParticipant(car, driver, team) {
    var _this = this;

    this.car = car;
    this.driver = driver;
    this.team = team;
    this.status = true;
    this.currentResult = 0;
    this.results = [];
    this.retirementLap = 0;
    this.total = 0;
    this.laps = [];

    this.getTotal = function () {
      return _this.total;
    };

    this.getBestValue = function () {
      var _this$best$result, _this$best;

      return (_this$best$result = (_this$best = _this.best) == null ? void 0 : _this$best.result()) != null ? _this$best$result : 0;
    };

    this.getBest = function () {
      return _extends({}, _this.best);
    };

    this.getLaps = function () {
      return [].concat(_this.laps);
    };

    this.valuesProvider = injector.resolve('EventValuesProvider');
    this.strategy = new Strategy(this.valuesProvider.getPitLap());
  }

  var _proto = SessionParticipant.prototype;

  _proto.addLap = function addLap(lap) {
    if (lap.num === 0) {
      this.total += lap.result();
      return;
    }

    if (lap.num === this.retirementLap) {
      this.status = false;
      return;
    }

    if (lap.num === this.strategy.pitLap) {
      lap.doPitStop();
      console.log(this.driver.name, 'on pit', lap.num);
    }

    this.laps.push(lap);

    if (!this.best || this.best.result() < lap.result()) {
      this.best = lap;
    }

    this.total += lap.result();
  };

  _proto.overtakeSuccess = function overtakeSuccess() {
    var _this$laps;

    (_this$laps = this.laps[this.laps.length - 1]) == null ? void 0 : _this$laps.overtakeSuccessPenalty();
  };

  _proto.wasOvertaken = function wasOvertaken() {
    var _this$laps2;

    (_this$laps2 = this.laps[this.laps.length - 1]) == null ? void 0 : _this$laps2.overtakeTargetPenalty();
  };

  _proto.overtakeFailed = function overtakeFailed() {
    var _this$laps3;

    (_this$laps3 = this.laps[this.laps.length - 1]) == null ? void 0 : _this$laps3.overtakeFailed();
  };

  _proto.verifyRetirement = function verifyRetirement(sessionType) {
    var res;

    switch (sessionType) {
      case SessionTypes.Practice:
        res = this.valuesProvider.retireFromPractice();
        break;

      case SessionTypes.Qualification:
        res = this.valuesProvider.retireFromQualification();
        break;

      case SessionTypes.Race:
        res = this.valuesProvider.retireFromRace();
        break;
    }

    if (res) {
      this.retirementLap = this.valuesProvider.getRetirementLap(sessionType);
    }
  };

  return SessionParticipant;
}();

var Team = /*#__PURE__*/function () {
  function Team(id, name, car, driver1, driver2) {
    this.id = id;
    this.name = name;
    this.car = car;
    this.driver1 = driver1;
    this.driver2 = driver2;
  }

  var _proto = Team.prototype;

  _proto.traceTeamInfo = function traceTeamInfo() {
    return this.name + " " + this.id;
  };

  return Team;
}();

var Track = /*#__PURE__*/function () {
  function Track(id, name, aero, chassis, engine) {
    this.id = id;
    this.name = name;
    this.aero = aero;
    this.chassis = chassis;
    this.engine = engine;
  }

  var _proto = Track.prototype;

  _proto.traceTrackInfo = function traceTrackInfo() {
    return this.name + " id: " + this.id + " aero: " + this.aero + " chassis: " + this.chassis + " engine: " + this.engine;
  };

  return Track;
}();

var Lap = /*#__PURE__*/function () {
  function Lap(num, sessionParticipant, track) {
    this.num = num;
    this.sessionParticipant = sessionParticipant;
    this.track = track;
    this.lapResult = 0;
    this.valuesProvider = injector.resolve('EventValuesProvider');
  }

  var _proto = Lap.prototype;

  _proto.doPracticeLap = function doPracticeLap() {
    this.lapResult = this.valuesProvider.getPracticeValue(this.sessionParticipant, this.track);
  };

  _proto.doQualificationLap = function doQualificationLap() {
    this.lapResult = this.valuesProvider.getQualValue(this.sessionParticipant, this.track);
  };

  _proto.doGridLap = function doGridLap(index) {
    this.lapResult = this.valuesProvider.getGridValue() * (index + 1);
  };

  _proto.doRaceLap = function doRaceLap() {
    this.lapResult = this.valuesProvider.getRaceValue(this.sessionParticipant, this.track);
  };

  _proto.overtakeFailed = function overtakeFailed() {
    this.lapResult -= this.valuesProvider.getOvertakeFailPenalty();
  };

  _proto.overtakeSuccessPenalty = function overtakeSuccessPenalty() {
    this.lapResult -= this.valuesProvider.getOvertakeSuccessPenalty();
  };

  _proto.overtakeTargetPenalty = function overtakeTargetPenalty() {
    this.lapResult -= this.valuesProvider.getOvertakeTargetPenalty();
  };

  _proto.doPitStop = function doPitStop() {
    this.lapResult -= this.valuesProvider.getPitStopPenalty();
  };

  _proto.result = function result() {
    return this.lapResult;
  };

  return Lap;
}();

var Session = /*#__PURE__*/function () {
  function Session(name, track, randomRange, sessionParticipants) {
    this.name = name;
    this.track = track;
    this.randomRange = randomRange;
    this.sessionParticipants = sessionParticipants;
  }

  var _proto = Session.prototype;

  _proto.sortByBest = function sortByBest() {
    this.sessionParticipants.sort(function (p1, p2) {
      return p2.getBestValue() - p1.getBestValue();
    });
  };

  _proto.sortByTotal = function sortByTotal() {
    this.sessionParticipants.sort(function (p1, p2) {
      return p2.getTotal() - p1.getTotal();
    });
  };

  _proto.traceResults = function traceResults() {
    return this.sessionParticipants.map(function (participant, index) {
      return index + 1 + "  " + participant.driver.name + "  " + participant.team.name + " " + participant.getBestValue() + " " + participant.getTotal();
    }).join('\n');
  };

  return Session;
}();

var Practice = /*#__PURE__*/function (_Session) {
  _inheritsLoose(Practice, _Session);

  function Practice(teams, track) {
    var _this;

    _this = _Session.call(this, 'practice', track, [0, 100], teams.reduce(function (acc, team) {
      acc.push(new SessionParticipant(team.car, team.driver1, team));
      acc.push(new SessionParticipant(team.car, team.driver2, team));
      return acc;
    }, [])) || this;
    _this.teams = teams;
    _this.track = track;
    return _this;
  }

  var _proto = Practice.prototype;

  _proto.doPractice = function doPractice() {
    var _this2 = this;

    var laps = 20;
    this.sessionParticipants.forEach(function (participant) {
      return participant.verifyRetirement(SessionTypes.Practice);
    });

    var _loop = function _loop(i) {
      _this2.sessionParticipants.forEach(function (participant) {
        var lap = new Lap(i, participant, _this2.track);
        lap.doPracticeLap();
        participant.addLap(lap);
      });
    };

    for (var i = 0; i < laps; i++) {
      _loop(i);
    }

    this.sortByBest();
  };

  return Practice;
}(Session);

var Qualification = /*#__PURE__*/function (_Session) {
  _inheritsLoose(Qualification, _Session);

  function Qualification(teams, track) {
    var _this;

    _this = _Session.call(this, 'qualification', track, [80, 100], teams.reduce(function (acc, team) {
      acc.push(new SessionParticipant(team.car, team.driver1, team));
      acc.push(new SessionParticipant(team.car, team.driver2, team));
      return acc;
    }, [])) || this;
    _this.teams = teams;
    _this.track = track;
    return _this;
  }

  var _proto = Qualification.prototype;

  _proto.doQualification = function doQualification() {
    var _this2 = this;

    var laps = 12;

    var _loop = function _loop(i) {
      _this2.sessionParticipants.forEach(function (participant) {
        var lap = new Lap(i, participant, _this2.track);
        lap.doQualificationLap();
        participant.addLap(lap);
      });
    };

    for (var i = 0; i < laps; i++) {
      _loop(i);
    }

    this.sortByBest();
  };

  return Qualification;
}(Session);

var Race = /*#__PURE__*/function (_Session) {
  _inheritsLoose(Race, _Session);

  function Race(sessionParticipants, track) {
    var _this;

    _this = _Session.call(this, 'race', track, [50, 100], sessionParticipants) || this;
    _this.sessionParticipants = sessionParticipants;
    _this.track = track;
    _this.valuesProvider = injector.resolve('EventValuesProvider');

    _this.prepareGrid();

    return _this;
  }

  var _proto = Race.prototype;

  _proto.prepareGrid = function prepareGrid() {
    var _this2 = this;

    this.sessionParticipants.forEach(function (participant, index) {
      var gridLap = new Lap(0, participant, _this2.track);
      gridLap.doGridLap(index);
      participant.addLap(gridLap);
    });
    this.sortByTotal();
  };

  _proto.doRace = function doRace() {
    var _this3 = this;

    var laps = 50;

    var _loop = function _loop(i) {
      _this3.sessionParticipants.forEach(function (participant) {
        if (!participant.status) {
          return;
        }

        var lap = new Lap(i, participant, _this3.track);
        lap.doRaceLap();
        participant.addLap(lap);
      });

      _this3.sortByTotal();

      _this3.sessionParticipants.forEach(function (participant, index) {
        if (!participant.status) {
          return;
        }

        var nextOne = _this3.sessionParticipants[index + 1];

        if (nextOne && _this3.collisionChecker(participant, nextOne)) {
          if (_this3.valuesProvider.wasOvertakeSuccessful()) {
            participant.overtakeSuccess();
            nextOne.wasOvertaken();
          } else {
            if (_this3.valuesProvider.wasCrashed()) {
              participant.status = false;
              nextOne.status = false;
            } else {
              participant.overtakeFailed();
            }
          }
        }
      });

      _this3.sortByTotal();

      _this3.printLap(i + 1);
    };

    for (var i = 0; i < laps; i++) {
      _loop(i);
    }
  };

  _proto.printLap = function printLap(num) {
    console.log("---------Lap " + num + "------------");
    var result = this.sessionParticipants.map(function (sp) {
      return sp.driver.name + " " + sp.team.name + " " + sp.getBestValue().toFixed(2) + " " + Math.floor(sp.getTotal()) + " " + sp.status;
    }).join('\n');
    console.log(result);
  };

  _proto.collisionChecker = function collisionChecker(participant1, participant2) {
    return participant1.getTotal() - participant2.getTotal() < 5;
  };

  return Race;
}(Session);

var MathUtil = /*#__PURE__*/function () {
  function MathUtil() {}

  MathUtil.getRandomBetween = function getRandomBetween(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
  };

  return MathUtil;
}();

var EventValuesProviderClass = /*#__PURE__*/function () {
  function EventValuesProviderClass(config) {
    var _this = this;

    this.config = config;

    this.getPitStopPenalty = function () {
      return _this.config.pitStopPenalty;
    };

    this.getOvertakeSuccessPenalty = function () {
      return _this.config.overtakeSuccessPenalty;
    };

    this.getOvertakeFailPenalty = function () {
      return _this.config.overtakeFailPenalty;
    };

    this.getOvertakeTargetPenalty = function () {
      return _this.config.overtakeTargetPenalty;
    };

    this.getGridValue = function () {
      return _this.config.gridLapValue;
    };
  }

  var _proto = EventValuesProviderClass.prototype;

  _proto.getPracticeValue = function getPracticeValue(sessionParticipant, track) {
    var base = this.calculateBase(sessionParticipant, track);
    return MathUtil.getRandomBetween(this.config.practiceMin, this.config.practiceMax) * base / 100;
  };

  _proto.getQualValue = function getQualValue(sessionParticipant, track) {
    var base = this.calculateBase(sessionParticipant, track);
    return MathUtil.getRandomBetween(this.config.qualificationMin, this.config.qualificationMax) * base / 100;
  };

  _proto.getRaceValue = function getRaceValue(sessionParticipant, track) {
    var base = this.calculateBase(sessionParticipant, track);
    return MathUtil.getRandomBetween(this.config.raceMin, this.config.raceMax) * base / 100;
  };

  _proto.wasOvertakeSuccessful = function wasOvertakeSuccessful() {
    return this.baseProbability() < this.config.overtakeProbability;
  };

  _proto.wasCrashed = function wasCrashed() {
    return this.baseProbability() < this.config.crashProbability;
  };

  _proto.retireFromPractice = function retireFromPractice() {
    return this.baseProbability() < this.config.techRetirementPractice;
  };

  _proto.getRetirementLap = function getRetirementLap(sessionType) {
    var res;

    switch (sessionType) {
      case SessionTypes.Practice:
        res = MathUtil.getRandomBetween(1, this.config.practiceLaps);
        break;

      case SessionTypes.Qualification:
        res = MathUtil.getRandomBetween(1, this.config.qualificationLaps);
        break;

      case SessionTypes.Race:
        res = MathUtil.getRandomBetween(1, this.config.raceLaps);
        break;
    }

    return res;
  };

  _proto.retireFromQualification = function retireFromQualification() {
    return this.baseProbability() < this.config.techRetirementQual;
  };

  _proto.retireFromRace = function retireFromRace() {
    return this.baseProbability() < this.config.techRetirementRace;
  };

  _proto.getPitLap = function getPitLap() {
    var delta = MathUtil.getRandomBetween(1, 7);
    return this.config.raceLaps / 2 - 5 + delta;
  };

  _proto.calculateBase = function calculateBase(sessionParticipant, track) {
    return sessionParticipant.driver.power * (track.aero * sessionParticipant.car.aero + track.chassis * sessionParticipant.car.chassis + track.engine * sessionParticipant.car.engine) / 3000;
  };

  _proto.baseProbability = function baseProbability() {
    return MathUtil.getRandomBetween(0, 100);
  };

  return EventValuesProviderClass;
}();

var entity;
function EventValuesProvider(config) {
  if (!entity) {
    entity = new EventValuesProviderClass(config);
  }

  return entity;
}

exports.Car = Car;
exports.Driver = Driver;
exports.EventValuesProvider = EventValuesProvider;
exports.Practice = Practice;
exports.Qualification = Qualification;
exports.Race = Race;
exports.SessionParticipant = SessionParticipant;
exports.Team = Team;
exports.Track = Track;
exports.injector = injector;
//# sourceMappingURL=cevert-lib.cjs.development.js.map
