declare class Injector {
    protected serviceInstanceMap: any;
    resolve<T>(name: string): T;
    register<T>(name: string, entity: T): void;
}
export declare const injector: Injector;
export {};
