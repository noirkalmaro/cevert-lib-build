import { SessionParticipant } from "../entities/sessionParticipant/sessionParticipant";
import { Track } from "../entities/track/track";
import { SessionTypes } from "../events/events.enum";
export interface EventValuesConfig {
    practiceMin: number;
    practiceMax: number;
    qualificationMin: number;
    qualificationMax: number;
    raceMin: number;
    raceMax: number;
    pitStopPenalty: number;
    overtakeSuccessPenalty: number;
    overtakeFailPenalty: number;
    overtakeTargetPenalty: number;
    gridLapValue: number;
    techRetirementPractice: number;
    techRetirementQual: number;
    techRetirementRace: number;
    crashProbability: number;
    overtakeProbability: number;
    practiceLaps: number;
    qualificationLaps: number;
    raceLaps: number;
}
declare class EventValuesProviderClass {
    private readonly config;
    constructor(config: EventValuesConfig);
    getPracticeValue(sessionParticipant: SessionParticipant, track: Track): number;
    getQualValue(sessionParticipant: SessionParticipant, track: Track): number;
    getRaceValue(sessionParticipant: SessionParticipant, track: Track): number;
    wasOvertakeSuccessful(): boolean;
    wasCrashed(): boolean;
    retireFromPractice(): boolean;
    getRetirementLap(sessionType: SessionTypes): number;
    retireFromQualification(): boolean;
    retireFromRace(): boolean;
    getPitLap(): number;
    getPitStopPenalty: () => number;
    getOvertakeSuccessPenalty: () => number;
    getOvertakeFailPenalty: () => number;
    getOvertakeTargetPenalty: () => number;
    getGridValue: () => number;
    protected calculateBase(sessionParticipant: SessionParticipant, track: Track): number;
    protected baseProbability(): number;
}
export declare function EventValuesProvider(config: EventValuesConfig): EventValuesProvider;
export declare type EventValuesProvider = EventValuesProviderClass;
export {};
